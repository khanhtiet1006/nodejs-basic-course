const students = require('./students');

// Map Function
function mapStudentsByName(array) {
  return array.map((student) => (student.name));
}

// Print Function 
function printResult(array) {
  console.log(`Expected Array Output: ${ array }`);
}

function main(array) {
  printResult(mapStudentsByName(array));
}

main(students);