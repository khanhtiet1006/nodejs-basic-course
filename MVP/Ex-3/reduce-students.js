const students = require('./students');

// For Each Solution without validation
function countNumberByGenderUsingForEach(array, gender) {
  let count = 0;
  array.forEach((student) => {
    if(student.gender === gender) {
      count++;
    }
  });
  return count;
} 

// Reduce Solution without validation
function countNumberByGenderUsingReduce(array, gender) {
  return array.reduce((total, student) => (student.gender === gender ? total + 1 : total), 0);
} 

// Print Function 
function printResult(gender, number) {
  console.log(`Number of ${ gender } students: ${ number }`);
}

// Simple Testing Case
function mainForEach(array, gender) {
  let numberOfMaleStudents = countNumberByGenderUsingForEach(array, gender);
  printResult(gender, numberOfMaleStudents);
}

function mainReduce(array, gender) {
  let numberOfMaleStudents = countNumberByGenderUsingReduce(array, gender);
  printResult(gender, numberOfMaleStudents);
}

mainForEach(students, 'female');
mainReduce(students, 'male');